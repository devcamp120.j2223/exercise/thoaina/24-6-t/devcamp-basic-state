import React from "react";

class Count extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            count: 0
        }
        // this.onBtnClickHandle = this.onBtnClickHandle.bind(this);
    }

    onBtnClickHandle = () => {
        this.setState({
            count: this.state.count + 1
        })
    }

    render() {
        return (
            <div>
                <p>Số lần click: {this.state.count} lần</p>
                <button onClick={this.onBtnClickHandle}>Click me!</button>
            </div>
        )
    }
}

export default Count;