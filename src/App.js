import Count from "./components/Count";


function App() {
  return (
    <div className="App">
      <Count init={3}></Count>
    </div>
  );
}

export default App;
